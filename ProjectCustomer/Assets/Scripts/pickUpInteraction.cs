﻿using UnityEngine;

public class pickUpInteraction : MonoBehaviour
{

    public GameObject DroppOff;
    private GameObject node;
    private GameObject spawner;
    private SpawnHandler spawnHandler;
    private float t;
    private float maxT;
    // Update is called once per frame
    void Update()
    {
        timer();
    }

    private void timer()
    {
        t += Time.deltaTime;
        if (t > maxT)
        {

            //run out of time
            Debug.Log("run out of time");

            //spawn car befor destroying
            getDestroyed();
        }
    }
    public void PickUp()
    {
        Debug.Log("passenger Got Picked Up");


        //Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 50);
        //GameObject.Instantiate(DroppOff, newPosition, transform.rotation);



        Debug.Log("destroying Pick Up area");



        spawnHandler.SpawnNewDropOff();
        getDestroyed();

    }
    public void getDestroyed()
    {


        foreach (GameObject otherObject in spawnHandler.existingPickUpList)
        {
            if (otherObject.transform.position == this.transform.position)
            {
                spawnHandler.existingPickUpList.Remove(otherObject);
            }
        }

        Destroy(transform.gameObject);
    }
    public void getSpawners(SpawnHandler spawner, float maxTime)
    {
        maxT = maxTime;
        spawnHandler = spawner;

    }
}
