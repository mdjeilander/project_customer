﻿using System.Collections.Generic;
using UnityEngine;
public class pathFinder : MonoBehaviour
{
    private Stack<Node> pathStack = new Stack<Node>();
    private bool _foundGoal = false;
    private Node _startNode;
    private Node _currentNode;
    private Node _goalNode;
    private List<GameObject> pathList = new List<GameObject>();


    void Start()
    {

    }

    public List<GameObject> getPathDFS(Node startNode, Node goalNode)
    {
        clear();
        //Debug.Log("startNode" + startNode);
        //Debug.Log("goalNode" + goalNode);
        _foundGoal = false;
        _goalNode = goalNode;
        pathStack.Push(startNode);
        if (startNode == goalNode)
        {
            return pathList;
        }
        DFS();
        foreach (Node otherNode in pathStack)
        {
            pathList.Add(otherNode.transform.gameObject);
        }
        return pathList;
    }
    private void clear()
    {
        pathStack.Clear();
        pathList.Clear();
        _currentNode = null;

    }
    private void DFS()
    {
        if (!_foundGoal)
        {
            if (pathStack.Count == 0)
            {
            //    System.Console.WriteLine("path is Empty");
                return;
            }
            _currentNode = pathStack.Peek();
          //  Debug.Log("current node " + _currentNode);
            if (_currentNode == _goalNode)
            {
           //     Debug.Log("found Goal");
                _foundGoal = true;
           //     Debug.Log("pathLength" + pathStack.Count);
                if (pathList.Count == 0)
                {
                    foreach (Node otherNode in pathStack)
                    {
                        pathList.Add(otherNode.transform.gameObject);
                    }
                }
                return;
            }
            else
            {
                foreach (Node connectedNode in _currentNode.connectedNodes)
                {
                    if (!pathStack.Contains(connectedNode))
                    {
                     //   Debug.Log("adding Connected Node" + connectedNode);
                        pathStack.Push(connectedNode);
                        DFS();
                    //    Debug.Log("popping Node");
                        pathStack.Pop();
                    }
                }
            }
        }
    }
}
