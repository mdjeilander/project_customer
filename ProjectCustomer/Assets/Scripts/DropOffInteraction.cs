﻿using UnityEngine;
using UnityEngine.UI;
public class DropOffInteraction : MonoBehaviour
{
    private SpawnHandler spawnHandler;
    private scoreScript score;
    private Text scoreText;
    public GameObject otherObject;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void DropOff()
    {


        Debug.Log("passenger Got Dropped Off");


        foreach (GameObject otherObject in spawnHandler.existingDropOffList)
        {
            if (otherObject.transform.position == this.transform.position)
            {
                spawnHandler.existingDropOffList.Remove(otherObject);
                Debug.Log("removing drop off");
            }
        }
        Debug.Log("destroying Dropp Off area");
        spawnHandler.SpawnNewPickUP();

        PlayerPrefs.SetInt(("Score"), (PlayerPrefs.GetInt("Score") + 1));
        Destroy(transform.gameObject);
    }
    public void getSpawner(SpawnHandler spawn)
    {
        spawnHandler = spawn;
    }
}
