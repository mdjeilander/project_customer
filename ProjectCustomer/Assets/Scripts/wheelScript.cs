﻿using UnityEngine;

public class wheelScript : MonoBehaviour
{
    private Rigidbody rb;

    [Header("is Wheel")]
    public bool wheelFL;
    public bool wheelFR;
    public bool wheelRL;
    public bool wheelRR;

    [Header("Suspension")]
    [SerializeField] private float _restLenght = 0.5f;
    [SerializeField] private float _springTravel = 0.2f;
    [SerializeField] private float _springStiffness = 30000f;
    [SerializeField] private float _damperStiffness = 4000f;
    [SerializeField] private float _breakStrength = 0.5f;

    private float _minLength;
    private float _maxLength;
    private float _springLength;
    private float _springForce;
    private float _lastLength;
    private float _damperForce;
    private float _springVelocity;

    private Vector3 _suspensionForce;
    private Vector3 _wheelVelocityLocalSpace;

    private float _forceX;
    private float _forceY;

    [Header("Wheel")]
    [SerializeField] private float _wheelRadius = 0.33f;
    public float steerAngle;
    private float _wheelAngle;
    [SerializeField] private float _steerTime = 0f;


    void Start()
    {
        rb = transform.root.GetComponent<Rigidbody>();

        _minLength = _restLenght - _springTravel;
        _maxLength = _restLenght + _springTravel;
    }
    private void Update()
    {
        _wheelAngle = Mathf.Lerp(_wheelAngle, steerAngle, _steerTime * Time.deltaTime);
        transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y + _wheelAngle, transform.localRotation.z);
        // Debug.Log(steerAngle);
        //  Debug.Log(transform.localRotation.y);
    }


    void FixedUpdate()
    {

        //Debug.Log(Input.GetAxis("PrimaryAttack"));
        if (Physics.Raycast(transform.position, -transform.up, out RaycastHit hit, _maxLength + _wheelRadius))
        {

            _lastLength = _springLength;
            _springLength = hit.distance - _wheelRadius;
            _springLength = Mathf.Clamp(_springLength, _minLength, _maxLength);
            _springVelocity = (_lastLength - _springLength) / Time.fixedDeltaTime;
            _springForce = _springStiffness * (_restLenght - _springLength);

            _damperForce = _damperStiffness * _springVelocity;


            _suspensionForce = (_springForce + _damperForce) * transform.up;

            _wheelVelocityLocalSpace = transform.InverseTransformDirection(rb.GetPointVelocity(hit.point));
            _forceX = Input.GetAxis("Vertical") * _springForce;
            _forceY = _wheelVelocityLocalSpace.x * _springForce;

            rb.AddForceAtPosition(_suspensionForce + (_forceX * -transform.forward) + (_forceY * -transform.right), hit.point);


            if (Input.GetAxis("Vertical") < 0f)
            {
                rb.velocity *= _breakStrength;
                //  rb.AddForceAtPosition(-transform.forward * _forceX * _breakStrength, hit.point);

            }

        }

    }
}
