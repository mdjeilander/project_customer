﻿using UnityEngine;

public class carInteraction : MonoBehaviour
{
    private int _passengerCount = 0;
    GameObject passenger;
    GameObject DropOff;
    public enum DeliveryState
    {
        PICKING_UP,
        DROPPING_OFF,
        DRIVING,
    }
    DeliveryState deliveryState = DeliveryState.DRIVING;

    // Start is called before the first frame update
    void Start()
    {
    }

    void Update()
    {

        //picking UP
        if (deliveryState == DeliveryState.PICKING_UP)
        {
            if (Input.GetKey("e") && _passengerCount < 4 || Input.GetKey("joystick button 2"))
            {
                //log pickUp, increase passenger Count, call pickUp function
                Debug.Log("picking Up passenger");
                _passengerCount += 1;
                pickUpInteraction pickUp = passenger.GetComponent<pickUpInteraction>();
                pickUp.PickUp();
                deliveryState = DeliveryState.DRIVING;
            }
        }
        else if (deliveryState == DeliveryState.DROPPING_OFF)
        {
            if (Input.GetKey("e") && _passengerCount > 0 || Input.GetKey("joystick button 2"))
            {
                Debug.Log("Dropping Off");
                _passengerCount -= 1;
                DropOffInteraction droppOff = DropOff.GetComponent<DropOffInteraction>();
                droppOff.DropOff();
                deliveryState = DeliveryState.DRIVING;

            }
        }
    }


    //handle hitbox off pickup and drop off area
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("trigger Enter");
        if (other.tag == "PickUp")
        {
            deliveryState = DeliveryState.PICKING_UP;

            passenger = other.transform.parent.gameObject;
            Debug.Log("Can Pick Up passenger");
        }
        else if (other.tag == "Delivery")
        {
            deliveryState = DeliveryState.DROPPING_OFF;
            DropOff = other.transform.parent.gameObject;
            Debug.Log("can drop of Passenger");
        }

    }
    //handle exiting off Pickup and dropp OFf zone
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PickUp")
        {
            deliveryState = DeliveryState.DRIVING;

            Debug.Log("Left Pick Up area");
        }
        else if (other.tag == "Delivery")
        {
            deliveryState = DeliveryState.DRIVING;
            Debug.Log("Left Delivery Area");
        }
    }
}
