﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnHandler : MonoBehaviour
{
    GameObject[] nodes;
    GameObject[] dropNodes;
    public GameObject PickUpObject;
    public GameObject DropOffObject;
    public List<GameObject> pickUpNodeList;
    public List<GameObject> dropNodeList;
    [Header("spawn Values (maxAmount < spawnNodes)")]
    [Header("( spawnNodes <=dropNodes)")]
    [SerializeField] private float startAmount = 0f;
    [SerializeField] private float maxAmount = 0f;
    [SerializeField] private float minAliveTime = 10f;
    [SerializeField] private float maxAliveTime = 0f;
    [SerializeField] private float timeTilNewSpawn = 0f;


    private float t = 0f;

    private int currentPickUpCount = 0;
    public List<GameObject> existingPickUpList;
    public List<GameObject> existingDropOffList;
    // Start is called before the first frame update
    void Start()
    {
        initNodes();

    }

    void initNodes()
    {
        //Create list
        existingPickUpList = new List<GameObject>();
        pickUpNodeList = new List<GameObject>();

        //find nodes
        nodes = GameObject.FindGameObjectsWithTag("NodeS");
        dropNodes = GameObject.FindGameObjectsWithTag("NodeD");

        //copy Arrays to lists
        foreach (GameObject otherNode in nodes)
        {
            Debug.Log("PickNode");
            pickUpNodeList.Add(otherNode);
        }
        foreach (GameObject otherNode in dropNodes)
        {
            Debug.Log("DropNode");
            dropNodeList.Add(otherNode);
        }

        //initial spawn
        for (int i = 0; i < startAmount; i++)
        {
            createSpawn();
        }
    }
    private void nextSpawn()
    {
        Debug.Log(t);
        t += Time.deltaTime;
        if (t > timeTilNewSpawn)
        {
            t = 0;
            createSpawn();
        }
    }




    public void createSpawn()
    {
        Debug.Log("spawning");
        //chose random position from list
        int indexer = Random.Range(0, pickUpNodeList.Count);
        Vector3 position = pickUpNodeList[indexer].transform.position;
        position.y += 0.001f;

        //create new pickUp
        GameObject gameObject = Instantiate(PickUpObject, position, transform.rotation);

        //check if there is a pick up in the chosen position
        foreach (GameObject otherObject in existingPickUpList)
        {
            if (otherObject.transform.position == gameObject.transform.position)
            {
                //delete crearted object and destroy old object
                Debug.Log("does contain created object");
                Destroy(gameObject);
                createSpawn();
                return;
            }
        }


        currentPickUpCount += 1;
        existingPickUpList.Add(gameObject);

        pickUpInteraction interaction = gameObject.GetComponent<pickUpInteraction>();
        interaction.getSpawners(this,maxAliveTime);

    }

    void Update()
    {
        nextSpawn();

        if (Input.GetKeyDown("x"))
        {
            SpawnNewPickUP();
        }
    }
    public void SpawnNewPickUP()
    {
        if (currentPickUpCount < maxAmount)
        {
            createSpawn();
        }
    }
    public void SpawnNewDropOff()
    {
        Debug.Log("spawning Drop Off");
        //chose random position from list
        int indexer = Random.Range(0, dropNodeList.Count);
        Vector3 position = dropNodeList[indexer].transform.position;
        position.y += 0.001f;

        //create new pickUp
        GameObject gameObject = Instantiate(DropOffObject, position, transform.rotation);

        //check if there is a pick up in the chosen position
        foreach (GameObject otherObject in existingDropOffList)
        {
            if (otherObject.transform.position == gameObject.transform.position)
            {
                //delete crearted object and destroy old object
                Debug.Log("does contain created object");
                Destroy(gameObject);
                SpawnNewDropOff();
                return;
            }
        }


        currentPickUpCount += 1;
        existingDropOffList.Add(gameObject);

        DropOffInteraction interaction = gameObject.GetComponent<DropOffInteraction>();
        interaction.getSpawner(this);

    }

    public void pickedUp(GameObject pickUp)
    {
        Debug.Log("count befor" + existingPickUpList.Count);

        existingPickUpList.Remove(pickUp);
        Debug.Log(" count after" + existingPickUpList.Count);
    }
    public void removObject()
    {

    }

}
