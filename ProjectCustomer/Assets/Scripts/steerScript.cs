﻿using System.Collections.Generic;
using UnityEngine;
public class steerScript : MonoBehaviour
{
    public List<wheelScript> _wheels;

    [Header("Specs")]
    //all in meters
    [SerializeField] private float _wheelBase = 2.55f;
    [SerializeField] private float _rearTrack = 1.6f;
    [SerializeField] private float _turnRadius = 9.8f;


    [Header("Inputs")]
    [SerializeField] private float _steerInput;


    [SerializeField] private float _ackermannAngleLeft;
    [SerializeField] private float _ackermannAngleRight;

    void Start()
    {

    }

    void Update()
    {

        _steerInput = Input.GetAxis("Horizontal");
        //turn right
        if (_steerInput > 0)
        {
            _ackermannAngleLeft = Mathf.Rad2Deg * Mathf.Atan(_wheelBase / (_turnRadius + (_rearTrack / 2))) * _steerInput;
            _ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(_wheelBase / (_turnRadius - (_rearTrack / 2))) * _steerInput;
        }
        //turn left
        else if (_steerInput < 0)
        {
            _ackermannAngleLeft = Mathf.Rad2Deg * Mathf.Atan(_wheelBase / (_turnRadius - (_rearTrack / 2))) * _steerInput;
            _ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(_wheelBase / (_turnRadius + (_rearTrack / 2))) * _steerInput;
        }
        //no Input --> angle =0 
        else
        {
            _ackermannAngleLeft = 0;
            _ackermannAngleRight = 0;
        }

        foreach (wheelScript wheel in _wheels)
        {
            if (wheel.wheelFL)
            {
                //   Debug.Log("setting Angle");
                wheel.steerAngle = _ackermannAngleLeft;
                //   Debug.Log("is FL" + wheel.steerAngle);
            }
            if (wheel.wheelFR)
            {
                //   Debug.Log("setting Angle");

                wheel.steerAngle = _ackermannAngleRight;
                //  Debug.Log("isFR" + wheel.steerAngle);

            }
        }
    }
}
