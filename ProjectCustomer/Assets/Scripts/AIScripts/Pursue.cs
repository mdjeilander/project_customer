﻿using UnityEngine;

public class Pursue : Seek
{
    public float maxPrediction;
    private GameObject _targetAux;
    private Agent _targetAgent;

    public override void Awake()
    {
        base.Awake();
        _targetAgent = target.GetComponent<Agent>();
        _targetAux = target;
        target = new GameObject();
    }

    private void OnDestroy()
    {
        Destroy(_targetAux);
    }
    public override Steering GetSteering()
    {
        Vector3 direction = _targetAux.transform.position - transform.position;
        float distance = direction.magnitude;
        float speed = agent.velocity.magnitude;
        float prediction;
        if (speed <= distance / maxPrediction)
        {
            prediction = maxPrediction;
        }
        else
        {
            prediction = distance / speed;
        }

        target.transform.position = _targetAux.transform.position;
        target.transform.position += _targetAgent.velocity * prediction;
        return base.GetSteering();
    }
}
