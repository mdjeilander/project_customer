﻿using UnityEngine;

public class targetMove : MonoBehaviour
{
    public float speed;
    public Vector3 position;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        position.z = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        position.x = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        transform.position = transform.position + position;
    }
}
