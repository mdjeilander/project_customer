﻿using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    public List<GameObject> otherNodeList = new List<GameObject>();
    public List<GameObject> nodes;
    public pathFinder pathFinder;
    private GameObject _startNode;
    private GameObject _lastNode;
    private GameObject _goalNode;
    private GameObject[] nodeArray;
    List<PathSegment> segments;

    private void Start()
    {

        pathFinder = GetComponent<pathFinder>();
        nodeArray = GameObject.FindGameObjectsWithTag("pathNode");
        foreach (GameObject otherNode in nodeArray)
        {
            nodes.Add(otherNode);
        }
        //chose random start and end of path
        initPathStart();

        //find path
        pathFinder = new pathFinder();
        Debug.Log("start" + _startNode);
        Debug.Log("goal" + _goalNode);
        otherNodeList = pathFinder.getPathDFS(_startNode.GetComponent<Node>(), _goalNode.GetComponent<Node>());
        _lastNode = _goalNode;
        segments = GetSegments();
    }

    private void initPathStart()
    {
        int startValue = Random.Range(0, nodes.Count);
        int goalValue = Random.Range(0, nodes.Count);
        //make sure end is unequal to end
        if (startValue == goalValue)
        {
            initPathStart();
            return;
        }
        _startNode = nodes[startValue];
        _goalNode = nodes[goalValue];
    }
    //get all segments
    public List<PathSegment> GetSegments()
    {
        //  Debug.Log("-----------------------");
        //   Debug.Log("getting segments");
        //  Debug.Log("-----------------------");

        foreach (GameObject otehrObject in otherNodeList)
        {
            //    Debug.Log(otehrObject.GetComponent<Node>());
        }
        List<PathSegment> segments = new List<PathSegment>();
        int i;
        for (i = 0; i < otherNodeList.Count - 1; i++)
        {
            Vector3 src = otherNodeList[i].transform.position;
            Vector3 dst = otherNodeList[i + 1].transform.position;
            PathSegment segment = new PathSegment(src, dst);
            segments.Add(segment);
        }
        return segments;
    }
    public float GetParam(Vector3 position, float lastParam)
    {
        float param = 0f;
        PathSegment currentSegment = null;
        float tempParam = 0f;
        foreach (PathSegment ps in segments)
        {
            tempParam += Vector3.Distance(ps.a, ps.b);
            if (lastParam <= tempParam)
            {
                currentSegment = ps;
                break;
            }
        }
        if (currentSegment == null)
        {
            return 0f;
        }
        Vector3 currentPos = position - currentSegment.a;
        Vector3 segmentDirection = currentSegment.b - currentSegment.a;
        segmentDirection.Normalize();
        Vector3 pointInSegment = Vector3.Project(currentPos, segmentDirection);

        param = tempParam - Vector3.Distance(currentSegment.a, currentSegment.b);
        param += pointInSegment.magnitude;

        return param;
    }
    public Vector3 GetPosition(float param)
    {
        Vector3 position = Vector3.zero;
        PathSegment currentSegment = null;
        float tempParam = 0f;
        foreach (PathSegment ps in segments)
        {
            tempParam += Vector3.Distance(ps.a, ps.b);
            if (param <= tempParam)
            {
                currentSegment = ps;
                break;
            }
        }
        if (currentSegment == null)
        {
            Debug.Log("reached End of Path");
            int goalValue = Random.Range(0, nodes.Count);
            _goalNode = nodes[goalValue];
            Debug.Log("start" + _startNode);
            Debug.Log("goal" + _goalNode);
            otherNodeList = pathFinder.getPathDFS(_startNode.GetComponent<Node>(), _goalNode.GetComponent<Node>());
            _startNode = _goalNode;
            segments = GetSegments();
            return Vector3.zero;
        }

        Vector3 segmentDirection = currentSegment.b - currentSegment.a;
        segmentDirection.Normalize();
        tempParam -= Vector3.Distance(currentSegment.a, currentSegment.b);

        tempParam = param - tempParam;
        position = currentSegment.a + segmentDirection * tempParam;
        return position;
    }

}
