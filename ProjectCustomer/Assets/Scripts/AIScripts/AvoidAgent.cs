﻿using UnityEngine;

public class AvoidAgent : AgentBehaviour
{
    public float collisionRadius;
    GameObject[] targets;

    private void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Agent");
    }

    public override Steering GetSteering()
    {
        Steering steering = new Steering();
        float shortestTime = Mathf.Infinity;
        GameObject firstTarget = null;
        float firstMinSeperation = 0.0f;
        float firstDistance = 0.0f;
        Vector3 firstRelativePos = Vector3.zero;
        Vector3 firstRelativeVelocity = Vector3.zero;
        foreach (GameObject t in targets)
        {
            Vector3 relativePos;
            Agent targetAgent = t.GetComponent<Agent>();
            relativePos = t.transform.position - transform.position;
            Vector3 relativeVelocity = targetAgent.velocity - agent.velocity;
            float relativeSpeed = relativeVelocity.magnitude;
            float timeToCollision = Vector3.Dot(relativePos, relativeVelocity);
            timeToCollision /= relativeSpeed * relativeSpeed * -1;
            float distance = relativePos.magnitude;
            float minSeparation = distance - relativeSpeed * timeToCollision;

            if (minSeparation > 2 * collisionRadius)
            {
                continue;
            }
            if (timeToCollision > 0.0f && timeToCollision < shortestTime)
            {
                shortestTime = timeToCollision;
                firstTarget = t;
                firstMinSeperation = minSeparation;
                firstRelativePos = relativePos;
                firstRelativeVelocity = relativeVelocity;
            }


            if (firstTarget == null)
            {
                return steering;
            }
            if (firstMinSeperation <= 0.0f || firstDistance < 2 * collisionRadius)
            {
                firstRelativePos = firstTarget.transform.position;
            }
            else
            {
                firstRelativePos += firstRelativeVelocity * shortestTime;
                firstRelativeVelocity.Normalize();
                steering.linear = -firstRelativeVelocity * agent.maxAccel;
                return steering;
            }
        }

        return base.GetSteering();
    }
}
