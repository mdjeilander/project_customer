﻿using UnityEngine;

public class Agent : MonoBehaviour
{
    public float maxRotation;
    public float maxSpeed;
    public float maxAccel;
    public float orientation;
    public float rotation;
    public float maxAngularAccel;
    public Vector3 velocity;
    protected Steering steering;
    private Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        velocity = Vector3.zero;
        steering = new Steering();
    }
    public virtual void FixedUpdate()
    {
        if (_rb == null)
        {
            return;
        }

        Vector3 displacement = velocity * Time.deltaTime;
        orientation += rotation * Time.deltaTime;
        if (orientation < 0.0f)
        {
            orientation += 360;
        }
        if (orientation > 360f)
        {
            orientation -= 360;
        }
        _rb.AddForce(displacement, ForceMode.VelocityChange);
        Vector3 orientationVector = OriToVec(orientation);
        _rb.rotation = Quaternion.LookRotation(orientationVector, Vector3.up);
    }

    public virtual void Update()
    {
        if (_rb == null)
        {
            return;
        }

        Vector3 displacement = velocity * Time.deltaTime;
        orientation += orientation * Time.deltaTime;
        if (orientation < 0.0f)
        {
            orientation += 360f;
        }
        else if (orientation > 360f)
        {
            orientation -= 360f;
        }

        transform.Translate(displacement, Space.World);
        transform.rotation = new Quaternion();
        transform.Rotate(Vector3.up, orientation);
    }
    public virtual void LateUpdate()
    {
        velocity += steering.linear * Time.deltaTime;
        rotation += steering.angular * Time.deltaTime;
        if (velocity.magnitude > maxSpeed)
        {
            velocity.Normalize();
            velocity *= maxSpeed;
        }
        if (steering.angular == 0.0f)
        {
            rotation = 0.0f;
        }
        if (steering.linear.sqrMagnitude == 0.0f)
        {
            velocity = Vector3.zero;
        }
        steering = new Steering();
    }
    public void SetSteering(Steering steering, float weight)
    {
        this.steering.linear += (weight * steering.linear);
        this.steering.angular += (weight * steering.angular);
    }

    public Vector3 OriToVec(float orientation)
    {
        Vector3 vector = Vector3.zero;
        vector.x = Mathf.Sin(orientation * Mathf.Deg2Rad) * 1.0f;
        vector.z = Mathf.Cos(orientation * Mathf.Deg2Rad) * 1.0f;
        return vector.normalized;
    }
}
