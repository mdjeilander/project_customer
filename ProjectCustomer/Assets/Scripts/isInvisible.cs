﻿using UnityEngine;

public class isInvisible : MonoBehaviour
{
    [SerializeField] bool isVisible = false;
    Renderer renderer;
    // Start is called before the first frame update
    void Start()
    {
        renderer = this.gameObject.GetComponent<Renderer>();
        if (!isVisible)
        {
            renderer.enabled = !renderer.enabled;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
