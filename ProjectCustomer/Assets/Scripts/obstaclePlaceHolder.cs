﻿using UnityEngine;

public class obstaclePlaceHolder : MonoBehaviour
{

    Vector3 newPosition = Vector3.zero;
    public float speed = 1f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        newPosition.z += speed * Time.deltaTime;
        transform.Translate(newPosition, Space.Self);
        Debug.Log(speed);
        if (transform.position.z > -19 || transform.position.z < -39)
        {
            transform.Rotate(Vector3.up, 180);

        }

    }
}
