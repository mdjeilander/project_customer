﻿using UnityEngine;
using UnityEngine.UI;
public class scoreScript : MonoBehaviour
{
    private int score = 0;
    private Text Text;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("Score", 0);
        Text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Text.text = PlayerPrefs.GetInt("Score").ToString();
        if (Input.GetKeyDown("i"))
        {
      
            PlayerPrefs.SetInt(("Score"), (PlayerPrefs.GetInt("Score") + 1));
        }
    }


}
